package org.kafken.try01;

public class MessagesConstrains {
    public static final String TOPIC_NAME = "getting-started";
    public static final String SERVER = "127.0.0.1:9092";
    public static final String CONSUMER_GROUP = "basic-consumer-sample";

    private MessagesConstrains() {

    }
}
