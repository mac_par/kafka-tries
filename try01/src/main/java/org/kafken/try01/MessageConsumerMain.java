package org.kafken.try01;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Map;
import java.util.Set;

public class MessageConsumerMain {
    private static final Logger logger = LoggerFactory.getLogger(MessageConsumerMain.class);

    public static void main(String[] args) throws InterruptedException {
        Map<String, Object> config = Map.of(
                ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, MessagesConstrains.SERVER,
                ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName(),
                ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName(),
                ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest",
                ConsumerConfig.GROUP_ID_CONFIG, MessagesConstrains.CONSUMER_GROUP,
                ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false
        );

        try (var consumer = new KafkaConsumer<String, String>(config)) {
            consumer.subscribe(Set.of(MessagesConstrains.TOPIC_NAME));
            logger.info("Starting to read messages from: {}", MessagesConstrains.TOPIC_NAME);

            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100L));
                for (var record : records) {
                    logger.info("Received: {} - {} at partition: {}", record.key(), record.value(), record.partition());
                }

                Thread.sleep(1000L);
            }
        }
    }
}
