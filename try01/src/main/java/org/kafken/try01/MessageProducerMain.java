package org.kafken.try01;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.Map;

public class MessageProducerMain {
    private static final Logger logger = LoggerFactory.getLogger(MessageProducerMain.class);

    public static void main(String[] args) throws InterruptedException {
        final var topicName = "getting-started";
        final Map<String, Object> config = Map.of(
                ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092",
                ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName(),
                ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName(),
                ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, true
        );

        try (var producer = new KafkaProducer<String, String>(config)) {
            while (true) {
                final var key = "myKey";
                final var value = LocalDateTime.now().toString();
                logger.info("Publishing: [{},{}]", key, value);
                producer.send(new ProducerRecord(topicName, key, value), ((metadata, exception) -> {
                    logger.warn("Published with metadata: {}, error: {}", metadata, exception);
                }));
                Thread.sleep(1000L);
            }
        }
    }
}
