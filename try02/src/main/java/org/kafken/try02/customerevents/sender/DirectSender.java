package org.kafken.try02.customerevents.sender;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.kafken.try02.customerevents.event.CustomerPayload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;

public class DirectSender implements EventSender {
    private static final Logger logger = LoggerFactory.getLogger(DirectSender.class);
    private final KafkaProducer<String, CustomerPayload> producer;
    private final String topic;

    public DirectSender(Map<String, Object> config, String topic) {
        this.topic = topic;
        Map<String, Object> mergedConfig = new HashMap<>(config);
        mergedConfig.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        mergedConfig.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, CustomerPayloadSerializer.class.getName());
        this.producer = new KafkaProducer<>(mergedConfig);
    }

    @Override
    public Future<RecordMetadata> send(CustomerPayload payload) {
        return producer.send(new ProducerRecord<>(topic, payload.getId().toString(), payload));
    }


    @Override
    public void close() {
        this.producer.close();
    }
}
