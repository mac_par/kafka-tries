package org.kafken.try02.customerevents.sender;

import org.apache.kafka.clients.producer.RecordMetadata;
import org.kafken.try02.customerevents.SendException;
import org.kafken.try02.customerevents.event.CustomerPayload;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public interface EventSender extends AutoCloseable {
    Future<RecordMetadata> send(CustomerPayload payload);

    default RecordMetadata blockingSend(CustomerPayload payload) throws SendException, InterruptedException {
        try {
            return send(payload).get();
        } catch (ExecutionException ex) {
            throw new SendException(ex);
        }
    }
}
