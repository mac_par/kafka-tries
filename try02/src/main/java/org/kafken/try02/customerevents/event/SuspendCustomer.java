package org.kafken.try02.customerevents.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.UUIDDeserializer;
import com.fasterxml.jackson.databind.ser.std.UUIDSerializer;

import java.util.UUID;

public class SuspendCustomer extends CustomerPayload {
    static final String TYPE = "SUSPEND_CUSTOMER";

    @JsonCreator
    public SuspendCustomer(@JsonSerialize(using = UUIDSerializer.class) @JsonDeserialize(using = UUIDDeserializer.class)
                           @JsonProperty("id") UUID id) {
        super(id);
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public String toString() {
        return String.format("%s[id=%s]", getClass().getSimpleName(), baseToString());
    }
}
