package org.kafken.try02.customerevents.sender;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.kafken.try02.customerevents.SendException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class RunRandomProducer {
    private static final Logger logger = LoggerFactory.getLogger(RunRandomProducer.class);

    public static void main(String[] args) throws InterruptedException, SendException {
        Map<String, Object> configProps = new HashMap<>();
        configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        configProps.put(ProducerConfig.CLIENT_ID_CONFIG, "customer_id_sample");
        final String topic = "customer-test";
        try (var sender = new DirectSender(configProps, topic)) {
            var businessLogic = new ProducerBusinessLogic(sender);
            while (true) {
                businessLogic.generateRandomizedData();
                Thread.sleep(500L);
            }
        }
    }
}
