package org.kafken.try02.customerevents.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Deserializer;
import org.kafken.try02.customerevents.event.CustomerPayload;

import java.io.IOException;

public class CustomerPayloadDeserializer implements Deserializer<CustomerPayloadOrError> {
    private final ObjectMapper objectMapper;

    public CustomerPayloadDeserializer() {
        this.objectMapper = new ObjectMapper();
    }

    @Override
    public CustomerPayloadOrError deserialize(String topic, byte[] data) {
        String encodedValue = new String(data);
        try {
            return new CustomerPayloadOrError(objectMapper.readValue(data, CustomerPayload.class), null, encodedValue);
        } catch (IOException e) {
            return new CustomerPayloadOrError(null, e, encodedValue);
        }
    }

}
