package org.kafken.try02.customerevents;

public class SendException extends Exception {
    public SendException(Throwable cause) {
        super(cause);
    }
}
