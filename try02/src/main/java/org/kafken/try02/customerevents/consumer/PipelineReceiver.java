package org.kafken.try02.customerevents.consumer;

import com.obsidiandynamics.worker.Terminator;
import com.obsidiandynamics.worker.WorkerExceptionHandler;
import com.obsidiandynamics.worker.WorkerOptions;
import com.obsidiandynamics.worker.WorkerThread;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.InterruptException;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class PipelineReceiver extends AbstractReceiver {
    private static final Logger logger = LoggerFactory.getLogger(PipelineReceiver.class);
    private final Consumer<String, CustomerPayloadOrError> consumer;
    private final Duration pollTimeout;
    private final WorkerThread pollWorker;
    private final WorkerThread processingWorker;
    private final BlockingQueue<ReceiveEvent> eventsQueue;
    private final Queue<Map<TopicPartition, OffsetAndMetadata>> pendingOffsetsQueue;

    public PipelineReceiver(String topicName, Duration pollTimeout, Map<String, Object> config, int queueCapacity) {
        this.eventsQueue = new LinkedBlockingQueue<>(queueCapacity);
        this.pendingOffsetsQueue = new LinkedBlockingQueue<>();
        this.pollTimeout = pollTimeout;
        Map<String, Object> mergedConfig = new HashMap<>(config);
        mergedConfig.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        mergedConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, CustomerPayloadDeserializer.class.getName());
        mergedConfig.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
        this.consumer = new KafkaConsumer<String, CustomerPayloadOrError>(mergedConfig);
        this.consumer.subscribe(Set.of(topicName));
        this.pollWorker = WorkerThread.builder()
                .withOptions(new WorkerOptions().daemon().withName(getClass(), "poller"))
                .onUncaughtException(this.uncaughtException())
                .onCycle(this::onPollCycle)
                .build();
        this.processingWorker = WorkerThread.builder()
                .withOptions(new WorkerOptions().daemon().withName(getClass(), "processing"))
                .onUncaughtException(this.uncaughtException())
                .onCycle(this::onProcessCycle)
                .build();
    }

    private void onPollCycle(WorkerThread t) throws InterruptedException {
        ConsumerRecords<String, CustomerPayloadOrError> records;

        try {
            records = consumer.poll(this.pollTimeout);
        } catch (InterruptException ex) {
            throw new InterruptedException(ex.getMessage());
        }

        if (records != null) {
            for (var record : records) {
                this.eventsQueue.put(new ReceiveEvent(record.value(), record));
            }
        }

        Map<TopicPartition, OffsetAndMetadata> pendingOffset = null;
        while ((pendingOffset = pendingOffsetsQueue.poll()) != null) {
            consumer.commitAsync(pendingOffset, null);
        }
    }

    private void onProcessCycle(WorkerThread t) throws InterruptedException {
        ReceiveEvent receiveEvent = this.eventsQueue.take();
        notifyListeners(receiveEvent);
        final var record = receiveEvent.getRecord();
        this.pendingOffsetsQueue.add(Map.of(new TopicPartition(record.topic(), record.partition()),
                new OffsetAndMetadata(record.offset() + 1)));
    }

    @Override
    public void start() {
        this.pollWorker.start();
        this.processingWorker.start();
    }

    @Override
    public void close() throws Exception {
        Terminator.of(pollWorker, processingWorker).terminate().joinSilently();
    }

    private WorkerExceptionHandler uncaughtException() {
        return (thread, exception) -> logger.warn("Uncaught exception: {} for thread: {}", exception.getMessage(), thread.getName());
    }
}
