package org.kafken.try02.customerevents.consumer;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

public class PipelineConsumer {
    private static final Logger logger = LoggerFactory.getLogger(PipelineConsumer.class);

    public static void main(String[] args) throws Exception {
        final Map<String, Object> config = new HashMap<>();
        config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        config.put(ConsumerConfig.GROUP_ID_CONFIG, "customer-pipelined-id");
        config.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        String topicName = "customer-test";
        Duration pollTimeout = Duration.ofMillis(100L);
        int queueCapacity = 10;

        try (PipelineReceiver consumer = new PipelineReceiver(topicName, pollTimeout, config, queueCapacity)) {
            new ConsumerBusinessLogic(consumer);
            logger.info("processing");
            consumer.start();
            Thread.sleep(10_000L);
            logger.info("finishing");
        }
    }
}
