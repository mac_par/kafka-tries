package org.kafken.try02.customerevents.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsumerBusinessLogic implements EventListener {
    private static final Logger logger = LoggerFactory.getLogger(ConsumerBusinessLogic.class);

    public ConsumerBusinessLogic(EventReceiver eventReceiver) {
        eventReceiver.addListener(this);
    }

    @Override
    public void onEvent(ReceiveEvent event) {
        if (event.isError()) {
            logger.error("Error event was received", event.getError());
        } else {
            logger.info("Receiver event: {}", event.getPayload());
        }
    }
}
