package org.kafken.try02.customerevents.sender;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Serializer;
import org.kafken.try02.customerevents.event.CustomerPayload;

public class CustomerPayloadSerializer implements Serializer<CustomerPayload> {
    private final ObjectMapper objectMapper;

    public CustomerPayloadSerializer() {
        this.objectMapper = new ObjectMapper();
    }

    @Override
    public byte[] serialize(String topic, CustomerPayload data) {
        try {
            return objectMapper.writeValueAsBytes(data);
        } catch (JsonProcessingException e) {
            throw new MarshallingException(e);
        }
    }

}
