package org.kafken.try02.customerevents.consumer;

import org.kafken.try02.customerevents.event.CustomerPayload;

import static java.lang.String.format;

public class CustomerPayloadOrError {
    private final CustomerPayload payload;
    private final Throwable error;
    private final String encodedValue;

    public CustomerPayloadOrError(CustomerPayload payload, Throwable error, String encodedValue) {
        this.payload = payload;
        this.error = error;
        this.encodedValue = encodedValue;
    }

    public CustomerPayload getPayload() {
        return payload;
    }

    public Throwable getError() {
        return error;
    }

    public String getEncodedValue() {
        return encodedValue;
    }

    @Override
    public String toString() {
        return format("%s[payload=%s. error=%s, encodedValue=%s]", payload, error, getEncodedValue());
    }
}
