package org.kafken.try02.customerevents.consumer;

public interface EventReceiver extends AutoCloseable {
    void addListener(EventListener eventListener);

    void start();

}
