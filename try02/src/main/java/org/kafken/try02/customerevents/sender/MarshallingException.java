package org.kafken.try02.customerevents.sender;

public class MarshallingException extends RuntimeException {
    public MarshallingException(Throwable cause) {
        super(cause);
    }
}
