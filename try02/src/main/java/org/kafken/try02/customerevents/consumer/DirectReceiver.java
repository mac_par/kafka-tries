package org.kafken.try02.customerevents.consumer;


import com.obsidiandynamics.worker.WorkerOptions;
import com.obsidiandynamics.worker.WorkerThread;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.InterruptException;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class DirectReceiver extends AbstractReceiver {
    private static final Logger logger = LoggerFactory.getLogger(DirectReceiver.class);
    private final WorkerThread pollingThread;
    private final KafkaConsumer<String, CustomerPayloadOrError> consumer;
    private final Duration pollTimeout;

    public DirectReceiver(Map<String, Object> config, String topic, Duration timeout) {
        Map<String, Object> mergedConfig = new HashMap<>(config);
        mergedConfig.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        mergedConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, CustomerPayloadDeserializer.class.getName());
        mergedConfig.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
        this.consumer = new KafkaConsumer<>(mergedConfig);
        this.consumer.subscribe(Set.of(topic));
        this.pollTimeout = timeout;
        this.pollingThread = WorkerThread.builder()
                .withOptions(new WorkerOptions().daemon().withName(getClass(), "poller"))
                .onCycle(this::onPollCycle)
                .build();
    }


    private void onPollCycle(WorkerThread t) throws InterruptedException {
        Optional<ConsumerRecords<String, CustomerPayloadOrError>> recordsOptional;
        try {
            recordsOptional = Optional.ofNullable(consumer.poll(pollTimeout));
        } catch (InterruptException ex) {
            logger.error("Exception pollin", ex);
            throw new InterruptException("Interrrupted during poll");
        }

        recordsOptional.ifPresent(records -> {
            for (var record : records) {
                final var payload = record.value();
                notifyListeners(new ReceiveEvent(payload, record));
            }
            consumer.commitAsync();
        });
    }

    @Override
    public void start() {
        this.pollingThread.start();
    }

    @Override
    public void close() {
        pollingThread.terminate().joinSilently();
        consumer.close();
    }
}
