package org.kafken.try02.customerevents.sender;

import org.kafken.try02.customerevents.SendException;
import org.kafken.try02.customerevents.event.CreateCustomer;
import org.kafken.try02.customerevents.event.CustomerPayload;
import org.kafken.try02.customerevents.event.ReinstateCustomer;
import org.kafken.try02.customerevents.event.SuspendCustomer;
import org.kafken.try02.customerevents.event.UpdateCustomer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

public class ProducerBusinessLogic {
    private static final Logger logger = LoggerFactory.getLogger(ProducerBusinessLogic.class);
    private final EventSender sender;

    public ProducerBusinessLogic(EventSender sender) {
        this.sender = sender;
    }

    public void generateRandomizedData() throws InterruptedException, SendException {
        final var create = new CreateCustomer(UUID.randomUUID(), "Bob", "Budowniczy");
        sending(create);
        if (Math.random() > 0.5) {
            sending(new UpdateCustomer(create.getId(), "Charlie", "Sheen"));
        }

        if (Math.random() > 0.5) {
            sending(new SuspendCustomer(create.getId()));
        }

        if (Math.random() > 0.5) {
            sending(new ReinstateCustomer(create.getId()));
        }
    }

    private void sending(CustomerPayload payload) throws SendException, InterruptedException {
        logger.info("Publishing {}", payload);
        sender.blockingSend(payload);
    }
}
