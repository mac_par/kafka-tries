package org.kafken.try02.customerevents.consumer;

import java.util.HashSet;
import java.util.Set;

public abstract class AbstractReceiver implements EventReceiver {
    private final Set<EventListener> listeners = new HashSet<>();

    protected AbstractReceiver() {

    }

    @Override
    public void addListener(EventListener eventListener) {
        listeners.add(eventListener);
    }

    protected void notifyListeners(ReceiveEvent event) {
        listeners.forEach(listener -> listener.onEvent(event));
    }
}
