package org.kafken.try02.customerevents.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.time.Duration;

public interface EventConsumer<K, V> extends AutoCloseable {
    ConsumerRecord<K, V> poll(Duration duration);
}
