package org.kafken.try02.customerevents.consumer;

import org.kafken.try02.customerevents.event.CustomerPayload;

@FunctionalInterface
public interface EventListener {
    void onEvent(ReceiveEvent event);
}
