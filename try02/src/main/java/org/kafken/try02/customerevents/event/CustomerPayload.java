package org.kafken.try02.customerevents.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.UUID;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = CreateCustomer.class, name = CreateCustomer.TYPE),
        @JsonSubTypes.Type(value = UpdateCustomer.class, name = UpdateCustomer.TYPE),
        @JsonSubTypes.Type(value = ReinstateCustomer.class, name = ReinstateCustomer.TYPE),
        @JsonSubTypes.Type(value = SuspendCustomer.class, name = SuspendCustomer.TYPE)
})
public abstract class CustomerPayload {
    @JsonProperty
    private UUID id;

    CustomerPayload(UUID id) {
        this.id = id;
    }

    public abstract String getType();

    public final UUID getId() {
        return this.id;
    }

    protected final String baseToString() {
        return this.id.toString();
    }
}
