package org.kafken.try02.customerevents.consumer;

public class UnMarshallingException extends RuntimeException {
    public UnMarshallingException(Throwable cause) {
        super(cause);
    }
}
