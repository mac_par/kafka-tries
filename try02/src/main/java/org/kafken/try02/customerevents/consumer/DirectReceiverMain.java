package org.kafken.try02.customerevents.consumer;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

public class DirectReceiverMain {
    private static final Logger logger = LoggerFactory.getLogger(DirectReceiverMain.class);

    public static void main(String[] args) throws InterruptedException {
        Map<String, Object> config = new HashMap<>();
        config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        config.put(ConsumerConfig.GROUP_ID_CONFIG, "customer-direct-consumer");
        config.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        final var topicName = "customer-test";

        logger.info("Initializing");
        try (var receiver = new DirectReceiver(config, topicName, Duration.ofMillis(100))) {
            new ConsumerBusinessLogic(receiver);
            receiver.start();
            Thread.sleep(10_000);
        }
    }
}
