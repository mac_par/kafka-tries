package org.kafken.try02.customerevents.event;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.UUIDDeserializer;
import com.fasterxml.jackson.databind.ser.std.UUIDSerializer;

import java.util.UUID;

public class UpdateCustomer extends CustomerPayload {
    static final String TYPE = "UPDATE_CUSTOMER";
    @JsonProperty
    private final String firstName;
    @JsonProperty
    private final String lastName;

    @JsonCreator
    public UpdateCustomer(@JsonSerialize(using = UUIDSerializer.class) @JsonDeserialize(using = UUIDDeserializer.class)
                          @JsonProperty("id") UUID id,
                          @JsonProperty("firstName") String firstName,
                          @JsonProperty("lastName") String lastName) {
        super(id);
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String getType() {
        return TYPE;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public String toString() {
        return String.format("%s[id=%s, firstName=%s, lastName=%s]", getClass().getSimpleName(),
                baseToString(), firstName, lastName);
    }
}
