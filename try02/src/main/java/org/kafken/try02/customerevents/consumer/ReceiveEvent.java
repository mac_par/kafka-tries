package org.kafken.try02.customerevents.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.kafken.try02.customerevents.event.CustomerPayload;

public class ReceiveEvent {
    private final CustomerPayload payload;
    private final Throwable error;
    private final String encodedValue;
    private final ConsumerRecord<String, CustomerPayloadOrError> record;

    public ReceiveEvent(CustomerPayload payload, Throwable error, ConsumerRecord<String, CustomerPayloadOrError> record, String encodedValue) {
        this.payload = payload;
        this.error = error;
        this.record = record;
        this.encodedValue = encodedValue;
    }

    public ReceiveEvent(CustomerPayloadOrError payload, ConsumerRecord<String, CustomerPayloadOrError> record) {
        this(payload.getPayload(), payload.getError(), record, payload.getEncodedValue());
    }

    public CustomerPayload getPayload() {
        return payload;
    }

    public Throwable getError() {
        return error;
    }

    public boolean isError() {
        return error != null;
    }

    public String getEncodedValue() {
        return encodedValue;
    }

    public ConsumerRecord<String, CustomerPayloadOrError> getRecord() {
        return record;
    }

    @Override
    public String toString() {
        return String.format("%s[payload=%s, error=%s, encodedValue=%s]", getClass().getSimpleName(),
                payload, error, encodedValue);
    }
}
